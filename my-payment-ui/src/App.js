import React from 'react';
import logo from './logo.svg';
import './App.css';
import "bootstrap/dist/css/bootstrap.min.css";
import RouterSwitch from "./components/Router"
import {BrowserRouter} from "react-router-dom";
import NavBar from "./components/Navigation"

function App() {
return (
      <div>
          <BrowserRouter>
            <NavBar/>
            <RouterSwitch/>
          </BrowserRouter>
    </div>
    );
}

export default App;
