import {PAYMENT_BY_TYPE_REQUEST, PAYMENT_BY_TYPE_FAIL, PAYMENT_BY_TYPE_SUCCESS, PAYMENT_BY_ID_REQUEST, PAYMENT_BY_ID_SUCCESS, PAYMENT_BY_ID_FAIL}
    from "../constants/PaymentConstants"
import PaymentRestService from "../../services/PaymentRestService";

const paymentByType = (x) => dispatch => {


    if (x.typevsID == "type") {
        try {
            console.log(">>> in dispatch ");
            dispatch({type: PAYMENT_BY_TYPE_REQUEST});
            const data = PaymentRestService.getbytype(x.paymenttype).then(response => {
                console.log(">>> in dispatch Payment List= + got data");
                dispatch({type: PAYMENT_BY_TYPE_SUCCESS, payload: response.data});
            });
        } catch
            (error) {
            console.log(">>> in dispatch error=" + error);
            dispatch({type: PAYMENT_BY_TYPE_FAIL, payload: error});
        }
    } else {
        try {
            console.log(x.paymentID);
            dispatch({type: PAYMENT_BY_ID_REQUEST});
            const data = PaymentRestService.getbyid(x.paymentID).then(response => {
                console.log(">>> in dispatch Payment List= + got data");
                console.log(response.data)
                dispatch({type: PAYMENT_BY_ID_SUCCESS, payload: response.data});
            });
        } catch
            (error) {
            console.log(">>> in dispatch error=" + error);
            dispatch({type: PAYMENT_BY_ID_FAIL, payload: error});
        }
    }
};

export default paymentByType;