import {
    PAYMENT_LIST_REQUEST, PAYMENT_LIST_SUCCESS, PAYMENT_LIST_FAIL,
    PAYMENT_BY_TYPE_REQUEST, PAYMENT_BY_TYPE_FAIL, PAYMENT_BY_TYPE_SUCCESS, PAYMENT_BY_ID_FAIL, PAYMENT_BY_ID_REQUEST, PAYMENT_BY_ID_SUCCESS
}
    from "../constants/PaymentConstants"

const paymentInitialState = {
    pending: false,
    payments: [],
    paymentsbyType: [],
    paymentsbyID: 0,
    error: null
}

function PaymentListReducer(state = paymentInitialState, action) {
    console.log("Reducer = " + action.type);

    switch (action.type) {
        case PAYMENT_LIST_REQUEST:
            console.log("PAYMENT_LIST");
            return {...state, pending: true};
        case PAYMENT_BY_TYPE_REQUEST:
            console.log("PAYMENT_BY_TYPE_REQUEST");
            return {...state, pending: true};
        case PAYMENT_BY_ID_REQUEST:
            console.log("PAYMENT_BY_ID_REQUEST");
            return {...state, pending: true};
        case PAYMENT_LIST_SUCCESS:
            console.log("PAYMENT_LIST_SUCCESS");
            return {...state, pending: false, payment: action.payload};
        case PAYMENT_BY_TYPE_SUCCESS:
            console.log("PAYMENT_BY_TYPE_SUCCESS");
            return {...state, pending: false, paymentsbyType: action.payload};
        case PAYMENT_BY_ID_SUCCESS:
            console.log("PAYMENT_BY_ID_SUCCESS");
            return {...state, pending: false, paymentsbyID: action.payload};
        case PAYMENT_LIST_FAIL:
            console.log("PAYMENT_LIST_FAIL");
            return {...state, pending: false, error: action.payload};
        case PAYMENT_BY_TYPE_FAIL:
            console.log("PAYMENT_BY_TYPE_FAIL");
            return {...state, pending: false, error: action.payload};
        case PAYMENT_BY_ID_FAIL:
            console.log("PAYMENT_BY_ID_FAIL");
            return {...state, pending: false, error: action.payload};
        default:
            return state;
    }
}

export default PaymentListReducer;

// import {
//     PAYMENT_LIST_REQUEST, PAYMENT_LIST_SUCCESS, PAYMENT_LIST_FAIL,
//     PAYMENT_BY_TYPE_REQUEST, PAYMENT_BY_TYPE_FAIL, PAYMENT_BY_TYPE_SUCCESS, PAYMENT_BY_ID_FAIL, PAYMENT_BY_ID_REQUEST, PAYMENT_BY_ID_SUCCESS
// }
//     from "../constants/PaymentConstants"
//
// const paymentInitialState = {
//     pending: false,
//     payments: [],
//     paymentsbyType: [],
//     paymentsbyID: 0,
//     error: null
// }
//
// function PaymentListReducer(state = paymentInitialState, action) {
//     console.log("Reducer = " + action.type);
//
//     switch (action.type) {
//         case PAYMENT_LIST_REQUEST:
//         case PAYMENT_BY_TYPE_REQUEST:
//         case PAYMENT_BY_ID_REQUEST:
//             console.log("Reducer 1");
//             return {...state, pending: true};
//         case PAYMENT_LIST_SUCCESS:
//             console.log(">>> reducer 2");
//             return {...state, pending: false, payment: action.payload};
//         case PAYMENT_BY_TYPE_SUCCESS:
//             console.log(">>> reducer 2");
//             return {...state, pending: false, paymentsbyType: action.payload};
//         case PAYMENT_BY_ID_SUCCESS:
//             console.log(">>> reducer 2");
//             return {...state, pending: false, paymentsbyID: action.payload};
//         case PAYMENT_LIST_FAIL:
//         case PAYMENT_BY_TYPE_FAIL:
//         case PAYMENT_BY_ID_FAIL:
//             console.log(">>> reducer 3");
//             return {...state, pending: false, error: action.payload};
//         default:
//             return state;
//     }
// }
//
// export default PaymentListReducer;
