import React, {Component, useState} from 'react';
import PaymentRestService from "../services/PaymentRestService";
import {Redirect} from 'react-router-dom';
import PropTypes from 'prop-types';
import DatePicker from "react-datepicker"
export default class extends Component {
    // int id, Date paymentdate, String type, double amount, int custid
    constructor(props) {
        super(props);
        this.onChangeId = this.onChangeId.bind(this);
        this.onChangePaymentDate = this.onChangePaymentDate.bind(this);
        this.onChangeType = this.onChangeType.bind(this);
        this.onChangeAmount = this.onChangeAmount.bind(this);
        this.onChangeCustId = this.onChangeCustId.bind(this);
        this.savePayment = this.savePayment.bind(this);

        this.state = {
            id: 0,
            paymentDate: "",
            type: "",
            amount:0,
            custid:0,
            submitted: false
        };
    }


    onChangeId(e) {
        this.setState({
            id: e.target.value
        });
    }

    onChangePaymentDate(e) {
        this.setState({
            paymentDate: e.target.value
        });
    }

    onChangeType(e) {
        this.setState({
            type: e.target.value
        });
    }

    onChangeAmount(e) {
        this.setState({
            amount: e.target.value
        });
    }

    onChangeCustId(e) {
        this.setState({
            custid: e.target.value
        });
    }

    savePayment() {
        var payment = {
            id: this.state.id,
            paymentDate: this.state.paymentDate,
            type: this.state.type,
            amount: this.state.amount,
            custid: this.state.custid        }
            console.log(payment);
        //debugger;


       PaymentRestService.create(payment)
            .then(response => {
                this.setState({
                    submitted: true

                });
                //debugger;

            })
            .catch(e => {
                alert(e);
                console.log(e);
            });
    }

    render() {
        if (this.state.submitted) {
            return <Redirect to= "home" />
        }
        return (<form>
            <h1>Add Payments</h1>
            <div className="form-group">
                <label htmlFor="exampleId">Id</label>
                <input type="number" className="form-control" id="exampleId" aria-describedby="idHelp"
                       placeholder="Enter id" value={this.state.id} onChange={this.onChangeId}/>
                <small id="idHelp" className="form-text text-muted">We'll never share your details with anyone
                    else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="examplePaymentDate">Payment Date</label>
                <input type="date" className="form-control" id="examplePaymentDate" aria-describedby="paymentDateHelp"
                       placeholder="Enter payment Date" value={this.state.paymentDate} onChange={this.onChangePaymentDate}/>
                {/*<DatePicker className="form-control" id="examplePaymentDate"*/}
                {/*    selected={date}*/}
                {/*    onChange={handleDateChange}*/}
                {/*    showTimeSelect*/}
                {/*    dateFormat={"Pp"}*/}
                {/*    />*/}

            </div>
            <div className="form-group">
                <label htmlFor="exampleType">Type</label>
                <input type="type" className="form-control" id="exampleType" aria-describedby="typeHelp"
                       placeholder="Enter Payment Type" value={this.state.type} onChange={this.onChangeType}/>
                <small id="typeHelp" className="form-text text-muted">We'll never share your details with anyone
                    else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="exampleAmount">Amount</label>
                <input type="number" className="form-control" id="exampleAmount" aria-describedby="amountHelp"
                       placeholder="Enter payment amount" value={this.state.amount} onChange={this.onChangeAmount}/>
                <small id="amountHelp" className="form-text text-muted">We'll never share your salary with anyone
                    else.</small>
            </div>
            <div className="form-group">
                <label htmlFor="exampleCustId">Cust Id</label>
                <input type="number" className="form-control" id="exampleCustId" aria-describedby="custIdHelp"
                       placeholder="Enter Cust Id" value={this.state.custid} onChange={this.onChangeCustId}/>
            </div>
            <div className="form-group">

                <input type="submit" className="form-control"  onClick={this.savePayment}/>
            </div>

        </form>)
    }
}

