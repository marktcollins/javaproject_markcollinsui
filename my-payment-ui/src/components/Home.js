import React from 'react';
import Sterling from "../images/Sterling.jpg";
import myStyles from "../styles/MyStyles.css"

function Home(props) {
    return (
        <div>
            <h1>MC Payment System</h1>
            <h2>{props.appname}</h2>
            <img className="home-img" src={Sterling} height={"40%"} width={"40%"}/>
        </div>
    )
}
export default Home;