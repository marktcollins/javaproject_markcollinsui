import React from "react";
import {Link} from "react-router-dom";

export default function NavBar() {
    return (
        <nav className="navbar navbar-expand navbar-blue bg-light">
            <a href="/home" className="navbar-brand">
                Home
            </a>
            <div className="navbar-nav mr-auto">

                <li className="nav-item">
                    <Link to={"/add"} className="nav-link">
                        Add Payment
                    </Link>
                </li>
                <li className="nav-item">
                    <Link to={"/viewpayments"} className="nav-link">
                        View Payments
                    </Link>
                </li>
                <li className="nav-item">
                    <Link to={"/about"} className="nav-link">
                        About
                    </Link>
                </li>
            </div>
        </nav>
    );
}