import React, {Component} from 'react';
import paymentByType from "../redux/actions/FindActions";
import connect from "react-redux/lib/connect/connect";

class ViewPaymentDetails extends Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        if (this.props.match.params.id) {
            try {
                var viewbyType = {paymentID: this.props.match.params.id, typesvsID: "ID"}
                console.log("the ID is: " + viewbyType.paymentID)
                this.props.dispatch(viewbyType)
            } catch (err) {
                console.log("no params passed: " + err)
                this.setState({error: true})
            }
        }
    }

    render() {
        const {paymentsByID} = this.props;
        console.log(this.props)
        return (
            <div>
                <h2>Payment Details</h2>
                <div className={"container"}>
                    {(paymentsByID)?(
                        <div className="card mb-3">
                            <div className="card-body">
                                <p className="card-text"><u><b>Payment Details</b></u><br/>
                                    <b>Amount: </b>{paymentsByID.amount} <br/>
                                    <b>Id: </b>{paymentsByID.id} <br/>
                                    <b>Type: </b>{paymentsByID.type} <br/>
                                    <b>Cust Id: </b>{paymentsByID.custid} <br/>
                                    <b>Date: </b>{paymentsByID.paymentDate}<br/>
                                </p>
                            </div>
                        </div>): 0}
                </div>
            </div>

        )
    }
}

const mapDispatchToProps = (x) => (dispatch) => {
    return {
        dispatch: (x) => dispatch(paymentByType(x))
    };
};

const mapStateToProps = state => ({
    paymentsByID: state.paymentsbyID
})

export default connect(mapStateToProps, mapDispatchToProps)(ViewPaymentDetails);