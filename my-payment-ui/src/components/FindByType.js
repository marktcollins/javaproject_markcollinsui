import React, {Component} from "react";
import paymentByType from '../redux/actions/FindActions'
import connect from "react-redux/lib/connect/connect";

class FindByType extends Component {
    constructor(props) {
        super(props);
        this.getTypes = this.getTypes.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            path: "",
            paymentTypeArray: [0],
            selectedValue: "",
        }
        this.getTypes();
    }

    getTypes() {
        const {payments} = this.props;
        let paymentTypeArr = [];
        let uniqueTypes = [0];
        payments && payments.map((item, index) => (paymentTypeArr[index] = item.type.toString().toLowerCase()));
        uniqueTypes = Array.from(new Set(paymentTypeArr));
        return uniqueTypes;
    }

    handleChange(event) {
        this.setState({selectValue: event.target.value})
        console.log(event.target.value)
        let viewbyType = {paymenttype: event.target.value, typevsID: "type"}
        this.props.dispatch(viewbyType)
    }

    render() {
        const {paymentsByType, currentIndex} = this.props;
        console.log(this.props)
        return (
            <div>
                <div>
                    <select value={this.state.selectValue} onChange={this.handleChange}>
                        <option value="">Select Type</option>
                        {this.getTypes() && this.getTypes().map((x, y) => <option key={y}>{x}</option>)}</select>
                </div>
                <div className={"container"}> {paymentsByType &&
                paymentsByType.map((item, index) => (
                    <div className="card mb-3" key={index}>
                        <div className="card-body">
                            <p className="card-text"><h5><b>Payment Details</b></h5><br/>
                                <b>Type: </b>{item.type} <br/>
                                <b>Amount: </b>{item.amount} <br/>
                                <b>Date: </b>{item.paymentDate}<br/>
                            </p>
                        </div>
                    </div>
                ))}
                </div>
            </div>
        )
    }
}

const mapDispatchToProps = (x) => (dispatch) => {
    return {
        dispatch: (x) => dispatch(paymentByType(x))
    };
};

const mapStateToProps = state => ({
    payments: state.payment,
    paymentsByType: state.paymentsbyType
})

export default connect(mapStateToProps, mapDispatchToProps)(FindByType);