import React, {Component} from 'react';
import connect from 'react-redux/lib/connect/connect';
import paymentList from '../redux/actions/PaymentListActions'
// import '../styles/viewallpaymentscss.css'
import {Link} from "react-router-dom";
// import picture from '../scripts/commonScripts'


class ViewAllPayments extends Component {

    constructor(props) {
        super(props)
        this.props.dispatch(paymentList())
    }

    render() {
        const {payments, currentIndex} = this.props;
        console.log(this.props)

        return (<div>
            <div className={"container"}> {payments &&
            payments.map((item, index) => (
                <div className="card mb-3" key={index}>
                    <div className="card-body">
                        <p className="card-text"><h5><b>Payment Details</b></h5><br/>
                            <b>Amount: </b>{item.amount} <br/>
                            <b>Type: </b>{item.type} <br/>
                            <b>Customer Id: </b>{item.custid}<br/>
                        </p>
                        <a tag={Link} href={`/paymentdetails/${item.id}`}>Payment Details</a>
                    </div>
                </div>
            ))}
            </div>
        </div>)
    }
}


const mapDispatchToProps = (dispatch) => {
    return {
        dispatch: () => dispatch(paymentList())
    };
};

const mapStateToProps = state => ({
    payments: state.payment
})

export default connect(mapStateToProps, mapDispatchToProps)(ViewAllPayments);