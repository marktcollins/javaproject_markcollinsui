import {Route, Switch} from "react-router-dom";
import Home from "./Home";
import AddPayments from "./AddPayments";
import About from "./About";
import ViewPayments from "./ViewPayments";
import ViewPaymentDetails from "./ViewPaymentDetails";
import React from "react";

export default function RouterSwitch() {
    return (
        <div className="container mt-3">
            <Switch>
                <Route exact path={["/", "/home"]} component={Home} />
                <Route exact path="/add" component={AddPayments} />
                <Route exact path="/about" component={About} />
                <Route path="/viewpayments" component={ViewPayments} />
                <Route exact path="/paymentdetails/:id" component={ViewPaymentDetails}/>
            </Switch>
        </div>
    );
}