import React, {Component} from 'react';
import scrooge from "../images/scrooge.jpg";

export default class About extends Component
{
    constructor(props) {
        super(props);
        this.state = {name:"MC payment page", year: "2020", apistatus: "Unknown", rowCount: 0}
    }

    render(){
        return (<div><h1>This site is the {this.state.name}</h1>
            <h2>The year is {this.state.year}</h2>
            <p><b>Status: </b>{this.state.apistatus}</p>
            <p>This site is incredibly popular worldwide! There have been <b>{this.state.rowCount}</b> payments lodged to date</p>
            <img className="home-img" src={scrooge} height={"40%"} width={"40%"}/>
        </div>)
    }

    async componentDidMount() {
        var url="http://localhost:8080/api/status"
        let rowCountUrl="http://localhost:8080/api/rowcount"
        let textData
        let rowNums
        try {
            let response = await fetch(url);
            textData=await response.text();
            console.log('The data is ' + textData)
            let rowCountResponse = await fetch(rowCountUrl);
            rowNums = await  rowCountResponse.text();
            console.log('The data is ' + rowNums)
        }
        catch(e)
        {
            console.log('The error is ' + e.toString())
        }
        this.setState( {apistatus: textData, rowCount: rowNums});
        return this.setState;
    }
}
