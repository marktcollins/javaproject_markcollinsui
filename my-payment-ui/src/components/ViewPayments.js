import React, {Component} from "react";
import ViewAllPayments from "./ViewAllPayments";
import FindByType from "./FindByType";

export default class ViewBy extends Component {

    constructor(props) {
        super(props);
        this.onValueChange = this.onValueChange.bind(this);
        this.state = {selectedOption: "viewAll"}
    }


    onValueChange(event) {
        this.setState({
            selectedOption: event.target.value
        });
    }

    render() {
        return (
            <div>
                <div className={"container"}>
                    <h1>View Payments</h1>
                    <div className="form-check">
                        <input className="form-check-input"
                               type="radio"
                               name="viewbyRadios"
                               id="viewbyRadios1"
                               value="viewAll"
                               checked={this.state.selectedOption === "viewAll"}
                               onChange={this.onValueChange}/>
                        <label className="form-check-label" htmlFor="viewbyRadios1">
                            View All
                        </label>
                    </div>
                    <div className="form-check">
                        <input className="form-check-input"
                               type="radio"
                               name="viewbyRadios"
                               id="viewbyRadios2"
                               value="viewByType"
                               checked={this.state.selectedOption === "viewByType"}
                               onChange={this.onValueChange}/>
                        <label className="form-check-label" htmlFor="exampleRadios2">
                            View By Type
                        </label>
                    </div>
                </div>

                <div>
                    {(this.state.selectedOption == "viewAll") ? <ViewAllPayments/> : <FindByType/>}
                </div>
            </div>
        )
    }
}