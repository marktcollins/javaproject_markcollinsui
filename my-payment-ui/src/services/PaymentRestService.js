import http from "../http-common";

export class PaymentRestService {

    create(payment) {
        return http.post("/save", payment);
    }

    getAllPayments() {
        return http.get("/all");
    }

    getbyid(id) {
        return (http.get(`/findid/${id}`));
    }

    getbytype(type) {
        return (type.length) ? http.get(`/findpaymenttype/${type}`) : null;
    }

}

export default new PaymentRestService();

// import http from "../http-common"
//
//
// export class PaymentRestService {
//     getAll() {
//         return http.get("/all");
//     }
//     findById(id) {
//         return (http.get(`/findid/${id}`));
//     }
//     findByType(type) {
//         return http.get(`/findpaymenttype/${type}`);
//     }
//     create(payment) {
//             return http.post("/save", payment);
//         }
// }
//
// export default new PaymentRestService();